#!/usr/bin/env node

/*
 * AUTHOR: FiraxisGlow *
 * VERSION: 1.0 *
 * LICENSE: MIT *
 * DISCLAIMER: The author does not take responsibility for use of the bot. *
 * No warranty is provided by the author. *
 */

const tmi = require('tmi.js');
const { Client } = require('pg');
const { get, post } = require('./Helpers/http');
const { createSQL, createNewEmoteSQL } = require('./Helpers/sql');
const { get7tvEmotes, getBTTVEmotes, getFFZEmotes } = require('./emotes');
require('dotenv').config();

const db = new Client({
  host: process.env.PG_HOST,
  user: process.env.PG_USER,
  password: process.env.PG_PASSWORD,
  database: process.env.PG_DATABASE,
  port: process.env.PG_PORT,
});

// Authentication function for Twitch API
const authenticate = async () => {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
  };
  const params = {
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.CLIENT_SECRET,
    grant_type: 'client_credentials',
  };
  const result = await post('https://id.twitch.tv/oauth2/token', null, headers, params);
  return result;
};

// Getting broadcaster details from Twitch API with Auth function credentials
const getBroadcaster = async (credentials) => {
  const headers = {
    Authorization: `Bearer ${credentials.access_token}`,
    'Client-Id': process.env.CLIENT_ID,
  };
  const params = {
    login: process.env.BROADCASTER,
  };
  const result = await get('https://api.twitch.tv/helix/users', headers, params);
  // Assuming getting one broadcaster here and deploying a new instance of the bot for each channel
  return result.data[0];
};

// Getting current emotes from Twitch plugin API's
// FFZ unsupported currently AFAIK
const getEmotes = async (broadcaster) => {
  const sevenTv = await get7tvEmotes(broadcaster);
  const bttv = await getBTTVEmotes(broadcaster);
  const ffz = await getFFZEmotes(broadcaster);
  const emotes = { 
    '7tv': sevenTv.channelEmotes,
    bttv: bttv.channelEmotes, 
    ffz: ffz.channelEmotes,
  };
  const globalEmotes = {
    '7tv': sevenTv.globalEmotes,
    bttv: bttv.globalEmotes,
    ffz: ffz.globalEmotes, 
  }
  const query = createNewEmoteSQL(emotes);
  db.query(query);
  return {emotes, globalEmotes};
};

const parseMessage = (messages, broadcaster, client) => {
  const parsedMessages = [];
  let emoteCount = 0;
  let messageHolder = [];
  messages.forEach((m, i) => {
    if (emoteCount < 10) {
      messageHolder.push(m);
      if (i > 0) emoteCount += 1;
    } else {
      parsedMessages.push(messageHolder.join(' '));
      messageHolder = [];
      emoteCount = 0;
      messageHolder.push(m);
    }
  });
  parsedMessages.push(messageHolder.join(' '));

  for (let i = 0; i <= parsedMessages.length - 1; i += 1) {
    if (i === 0) {
      client.say(broadcaster.login, parsedMessages[i]);
    } else {
      // Function wrapping fuckery to pass the index properly
      ((index) => {
        // Stagger the messages with a timeout that's multiplied with the index
        // This spreads out the messages evenly without triggering twitch
        setTimeout(() => {
          client.say(broadcaster.login, parsedMessages[index]);
        }, 3000 * index);
      })(i);
    }
  }
};

const bot = async () => {
  await db.connect();
  const credentials = await authenticate();
  const broadcaster = await getBroadcaster(credentials);
  let { emotes, globalEmotes } = await getEmotes(broadcaster);
  let emoteLookupArray = emotes['7tv']
  .concat(emotes.bttv)
  .concat(emotes.ffz)
  .concat(globalEmotes['7tv'])
  .concat(globalEmotes.bttv)
  .concat(globalEmotes.ffz);
  let commandCooldown = false;

  const options = {
    options: {
      debug: true,
    },
    connection: {
      reconnect: true,
    },
    identity: {
      username: process.env.TWITCH_USERNAME,
      password: process.env.TWITCH_PASSWORD,
    },
    channels: [broadcaster.login],
  };
  const client = new tmi.Client({ ...options });
  client.connect().catch(console.error);
  client.on('message', async (channel, tags, message, self) => {
    if (self || tags.username === 'nightbot') return;

    const emoteCounts = { '7tv': {}, bttv: {}, ffz: {} };
    const words = message.split(' ');
    switch (words[0]) {
      case '!count': {
        if (
          // Check that command user is either a twitch mod or a developer
          // also enforce 5s command cooldown
          (tags.mod || tags.username === process.env.DEVELOPER)
          && !commandCooldown
        ) {
          commandCooldown = true;
          setTimeout(() => {
            commandCooldown = false;
          }, 5000);
          switch (words[1]) {
            case 'top': {
              if (!Number(words[2]).isNaN) {
                let noGlobals = '';
                if (words[3] && words[3] === 'no-globals') {
                  const globals = globalEmotes['7tv'].concat(globalEmotes.bttv).concat(globalEmotes.ffz);
                  noGlobals = `AND name NOT IN (${globals.map((e) => `'${e}'`).join(',')})`;
                }
                const res = await db.query(
                  `SELECT name, count FROM emotes
                  WHERE name IN (${emoteLookupArray.map((e) => `'${e}'`).join(',')}) ${noGlobals}
                  ORDER BY count DESC LIMIT ${words[2]}`,
                );
                if (res?.rows.length === Number(words[2])) {
                  const tMessage = [`Top ${words[2]} Emotes:`];
                  res.rows.forEach((r, i) => {
                    tMessage.push(`${i + 1}. ${r.name} ${r.count}`);
                  });
                  parseMessage(tMessage, broadcaster, client);
                }
              } else {
                client.say(broadcaster.login, 'Value is not a number.');
              }
              break;
            }
            case 'bottom': {
              if (!Number(words[2]).isNaN) {
                let noGlobals = '';
                if (words[3] && words[3] === 'no-globals') {
                  const globals = globalEmotes['7tv'].concat(globalEmotes.bttv).concat(globalEmotes.ffz);
                  noGlobals = `AND name NOT IN (${globals.map((e) => `'${e}'`).join(',')})`;
                }
                const res = await db.query(
                  `SELECT name, count FROM emotes
                  WHERE name IN (${emoteLookupArray.map((e) => `'${e}'`).join(',')}) ${noGlobals}
                  ORDER BY count ASC LIMIT ${words[2]}`,
                );
                if (res?.rows.length === Number(words[2])) {
                  const bMessage = [`Bottom ${words[2]} Emotes:`];
                  res.rows.forEach((r, i) => {
                    bMessage.push(`${i + 1}. ${r.name} ${r.count}`);
                  });
                  parseMessage(bMessage, broadcaster, client);
                } else {
                  client.say(broadcaster.login, 'Value is not a number.');
                }
              }
              break;
            }
            default: {
              const res = await db.query(`SELECT count FROM emotes WHERE name = '${words[1]}'`);
              if (res?.rows.length === 1) {
                client.say(
                  broadcaster.login,
                  `${words[1]} Has been used ${res?.rows[0]?.count} times.`,
                );
              } else {
                client.say(broadcaster.login, `${words[1]} Count not found.`);
              }
            }
          }
        }
        break;
      }
      case '!refresh': {
        // Check that refresh command is complete and that the user is a Twitch mod
        if (
          message === '!refresh emotes'
          && (tags.mod || tags.username === process.env.DEVELOPER)
        ) {
          const {emotes: newEmotes, globalEmotes: newGlobalEmotes } = await getEmotes(broadcaster);
          emotes = newEmotes;
          globalEmotes = newGlobalEmotes;
          emoteLookupArray = emotes['7tv']
          .concat(emotes.bttv)
          .concat(emotes.ffz)
          .concat(globalEmotes['7tv'])
          .concat(globalEmotes.bttv)
          .concat(globalEmotes.ffz);
        }
        break;
      }
      default: {
        const platforms = Object.keys(emotes);
        let emotesFound = false;

        words.forEach((w) => {
          if (emoteLookupArray.includes(w)) {
            emotesFound = true;
            platforms.forEach((key, i) => {
              if (emotes[key].includes(w) || globalEmotes[key].includes(w)) {
                // Annoyingly complicated duplicate detection
                if (
                  i === 0
                  || (i === 1 && !(w in emoteCounts[platforms[0]]))
                  || (i === 2
                    && !(w in emoteCounts[platforms[0]])
                    && !(w in emoteCounts[platforms[1]]))
                ) {
                  if (w in emoteCounts[key]) {
                    emoteCounts[key][w] += 1;
                  } else {
                    emoteCounts[key][w] = 1;
                  }
                }
              }
            });
          }
        });
        if (emotesFound) {
          const query = createSQL(emoteCounts);
          db.query(query);
        }
        break;
      }
    }
  });
};

bot();
