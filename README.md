# FiraxisBot

Twitch chat bot for counting twitch plugin emotes on a given channel.

## Installing the bot

- Clone this repositoty
- Run 
```
npm install
```
- If not installed, install PostgreSQL
- Write down the password of the db user you intend to use for the bot
- Open postgres on the terminal and run the following commands

```
CREATE TABLE emotes (
    id SERIAL PRIMARY KEY,
    name VARCHAR (500) UNIQUE,
    plugin VARCHAR (50),
    count BIGINT
);
```
```
ALTER TABLE emotes ADD CONSTRAINT unique_name UNIQUE (name);
```
- Rename .env.example to .env and replace the variables with values of your usecase

## Running the bot
- Use
```
npm start
```
- Or
```
node index.js
```

## Commands

- Top X emotes
```
!count top <amount> 
Exclude globals: !count top <amount> no-globals
```
- Bottom X emotes
```
!count bottom <amount>
Exclude globals: !count bottom <amount> no-globals
```
- Emote count
```
!count <Emote Name>
```
- Refreshing emote list
```
!refresh emotes
```