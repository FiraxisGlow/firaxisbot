const createSQL = (emotes) => {
  const sqlStrings = [
    'INSERT INTO emotes (name, plugin, count)',
    'VALUES',
    'ON CONFLICT ON CONSTRAINT unique_name DO UPDATE SET count = (emotes.count + excluded.count);',
  ];
  const platforms = Object.keys(emotes);
  // Set splicing index of sqlStrings after values
  let sqlIndex = 2;
  // Loop through twitch extensions
  platforms.forEach((platform) => {
    // Loop through the emotes of the extension
    const names = Object.keys(emotes[platform]);
    names.forEach((name, n) => {
      let lineEnding = ',';
      // Annoyingly complicated checks for SQL line ends for values
      if (
        platform === '7tv'
        && Object.keys(emotes.bttv).length === 0
        && Object.keys(emotes.ffz).length === 0
        && n + 1 === names.length
      ) {
        lineEnding = '';
      } else if (
        platform === 'bttv'
        && Object.keys(emotes.ffz).length === 0
        && n + 1 === names.length
      ) {
        lineEnding = '';
      } else if (platform === 'ffz' && n + 1 === names.length) {
        lineEnding = '';
      }
      // Add value row to sqlStrings
      sqlStrings.splice(
        sqlIndex,
        0,
        `('${name}', '${platform}', '${emotes[platform][name]}')${lineEnding}`,
      );
      sqlIndex += 1;
    });
  });
  // Join sqlStrings with row change into a single string
  return sqlStrings.join('\n');
};

const createNewEmoteSQL = (emotes) => {
  const sqlStrings = [
    'INSERT INTO emotes (name, plugin, count)',
    'VALUES',
    'ON CONFLICT ON CONSTRAINT unique_name DO NOTHING;',
  ];
  const platforms = Object.keys(emotes);
  // Set splicing index of sqlStrings after values
  let sqlIndex = 2;
  // Loop through twitch extensions
  platforms.forEach((platform, p) => {
    // Loop through the emotes of the extension
    emotes[platform].forEach((name, n) => {
      let lineEnding = ',';
      if (p + 1 === platforms.length) {
        // On last emote platform check that it's the last emote for linend change
        if (n + 1 === emotes[platforms[platforms.length - 1]].length) {
          lineEnding = '';
        }
      }
      // Add value row to sqlStrings
      sqlStrings.splice(sqlIndex, 0, `('${name}', '${platform}', '0')${lineEnding}`);
      sqlIndex += 1;
    });
  });
  // Join sqlStrings with row change into a single string
  return sqlStrings.join('\n');
};

module.exports = { createSQL, createNewEmoteSQL };
