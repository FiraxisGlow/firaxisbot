const { default: axios } = require('axios');

const get = async (url, headers, params) => {
  try {
    const result = await axios.get(url, {
      headers: headers && Object.keys(headers).length > 0 ? headers : undefined,
      params: params && Object.keys(params).length > 0 ? params : undefined,
    });
    const data = await result.data;
    return data;
  } catch (e) {
    throw new Error(JSON.stringify(e));
  }
};

const post = async (url, data, headers, params) => {
  try {
    const result = await axios.post(url, null, {
      headers: headers && Object.keys(headers).length > 0 ? headers : undefined,
      params: params && Object.keys(params).length > 0 ? params : undefined,
      data: data && Object.keys(data).length > 0 ? params : undefined,
    });
    const resultData = await result.data;
    return resultData;
  } catch (e) {
    throw new Error(JSON.stringify(e));
  }
};

module.exports = { get, post };
