/* eslint-disable consistent-return */
const { get } = require('./Helpers/http');

const get7tvEmotes = async (broadcaster) => {
  try {
    const channel = broadcaster?.login;
    const result = await get(`https://api.7tv.app/v2/users/${channel}/emotes`);
    const globalResult = await get('https://api.7tv.app/v2/emotes/global');
    const channelEmotes = result.map((e) => e.name);
    const globalEmotes = globalResult.map((ge) => ge.name);
    return {channelEmotes, globalEmotes};
  } catch (e) {
    console.log(e);
  }
};

const getBTTVEmotes = async (broadcaster) => {
  try {
    const channel = broadcaster?.login;
    const result = await get(`https://decapi.me/bttv/emotes/${channel}`);
    const globalResult = await get('https://api.betterttv.net/3/cached/emotes/global');
    const channelEmotes = result.split(' ');
    const globalEmotes = globalResult.map((ge) => ge.code);
    return {channelEmotes, globalEmotes};
  } catch (e) {
    console.log(e);
  }
};

const getFFZEmotes = async (broadcaster) => {
  try {
    const id = broadcaster?.id;
    const result = await get(`https://api.frankerfacez.com/v1/room/id/${id}`);
    const globalResult = await get('https://api.frankerfacez.com/v1/set/global');
    const sets = Object.keys(result.sets);
    const globalSets = Object.keys(globalResult.sets);
    const channelEmotes = [];
    const globalEmotes = [];
    sets.forEach((s) => {
      result.sets[s].emoticons.forEach((e) => {
        channelEmotes.push(e.name);
      });
    });
    globalSets.forEach((gs) => {
      globalResult.sets[gs].emoticons.forEach((ge) => {
        globalEmotes.push(ge.name);
      });
    });
    return {channelEmotes, globalEmotes};
  } catch (e) {
    console.log(e);
  }
};

module.exports = { get7tvEmotes, getBTTVEmotes, getFFZEmotes };
